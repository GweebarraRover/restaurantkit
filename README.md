//Restaurant Kit - 4 hours work approx. 

***How to use***
Drop framework into app and import into class.
Easily setup with default init.

let restaurant = Restaurant()

Filters can be obtained by calling 
filterDishOptions OR filterDietOptions
returns an array of options for both. These can be stored in the class and displayed to user for selection.

Menu Recipies are obtained by function
filterMenuBy(dishFilters:[DishFilter], dietFilters:[DietFilter])
This returns the menu based on the users selction and can be called like this 
        
        // Filter menu by Vegan friendly Soups or Desserts 
        restaurant.filterMenuBy(dishFilters: [.Soups, .Desserts], dietFilters: [.Vegan])




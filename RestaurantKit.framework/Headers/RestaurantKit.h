//
//  RestaurantKit.h
//  RestaurantKit
//
//  Created by Keelin Devenney on 17/12/2019.
//  Copyright © 2019 Keelin Devenney. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RestaurantKit.
FOUNDATION_EXPORT double RestaurantKitVersionNumber;

//! Project version string for RestaurantKit.
FOUNDATION_EXPORT const unsigned char RestaurantKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RestaurantKit/PublicHeader.h>


